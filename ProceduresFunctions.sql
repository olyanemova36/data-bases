CREATE OR REPLACE PROCEDURE get_user_reputation("id" integer) LANGUAGE plpgsql AS $$
    DECLARE
        attempts integer;
        violations integer;
        time_in_casino interval;
        additional_duration interval;
        awards decimal;

        BEGIN
            awards = (
                SELECT SUM(sum)
                FROM finance_operation
                WHERE "user" = "id" AND operation_type = 'payment');

            attempts = (
                SELECT COUNT(*) attempts
                FROM player_in_roundgame
                WHERE user_id = "id");

            violations = (
                SELECT COUNT(*) violations
                FROM blocker_player
                WHERE blocked_player_id = id);

            time_in_casino = (
                SELECT MAX(begin) - MIN(begin)
                FROM player_in_roundgame pir JOIN game_round gr on pir.round_id = gr.id_game_round
                WHERE user_id = "id");

            RAISE INFO 'The selected user % - played in % games - was blocked % times - earned %  money - spend %  ', "id", attempts, violations, awards, justify_days(time_in_casino);
            END;

$$;

CALL get_user_reputation(7);
CALL get_user_reputation(5);

CREATE OR REPLACE PROCEDURE generate_currency_back_ratio(day date) LANGUAGE plpgsql AS $$
    DECLARE
        currency_cursor REFCURSOR;
        is_exist BOOL;
        currency_info RECORD;
    BEGIN
        OPEN currency_cursor SCROLL FOR SELECT "from", "in", value FROM exchange_rate WHERE date = day;

        LOOP
            FETCH currency_cursor INTO currency_info;
            EXIT WHEN NOT FOUND;

            is_exist = (SELECT EXISTS(SELECT "from", "in", value
                                      FROM exchange_rate
                                      WHERE "from" = currency_info."in"
                                        AND "in" = currency_info."from"
                                        AND date = day));
            IF(is_exist) THEN
                CONTINUE;
            END IF;

            INSERT INTO exchange_rate VALUES
                (DEFAULT, currency_info."from", currency_info."in", day, 1 / currency_info.value);

        END LOOP;
    END
$$;

CALL generate_currency_back_ratio('2022-05-05');

CREATE OR REPLACE PROCEDURE generate_exchange_rate(curr currency_abr, begin decimal(10,4)) LANGUAGE plpgsql AS $$
    DECLARE
        rows INT;
        currencies currency_abr ARRAY;
    BEGIN
        rows = 0;
        currencies = ARRAY['CHIP', 'RUB', 'RUB', 'EUR', 'GBP', 'KRW', 'CNY', 'JPY'];
        LOOP
            EXIT WHEN rows = 8;
            rows = rows + 1;
            IF currencies[rows] = curr THEN
                CONTINUE;
            END IF;
            IF (SELECT EXISTS(SELECT "in", "from", date FROM exchange_rate
                WHERE "in" = curr AND "from" = currencies[rows] AND date = current_date) ) THEN
                CONTINUE;
            end if;
            INSERT INTO exchange_rate VALUES
            (DEFAULT,  curr, currencies[rows], current_date, begin + rows);
        END LOOP;
    END
$$;

CALL generate_exchange_rate('JPY', 700);

CREATE OR REPLACE PROCEDURE generate_users(count INT, type usertype) LANGUAGE plpgsql AS $$
    DECLARE
    BEGIN
        LOOP
            EXIT WHEN count = 0;
            count = count - 1;
            INSERT INTO "user" VALUES
            (DEFAULT, CONCAT('fict_', type, CAST(count as varchar(5))), type, CONCAT('avatar', CAST(count as varchar(5))),
                      CONCAT('hash', CAST(count as varchar(5))), CONCAT('salt', CAST(count as varchar(5))));
        END LOOP;
    END
$$;

CALL generate_users( 20, 'violator');
CALL generate_users( 10, 'admin');

CREATE OR REPLACE PROCEDURE generate_gameround(count INT) LANGUAGE plpgsql AS $$
    DECLARE
        gameplayers INT ARRAY;
        day DATE;
        rows INT;
        bett INT;
        game INT;
        adminn INT;
    BEGIN
        gameplayers = ARRAY[5, 3, 7, 0, 6, 4, 9, 10, 5, 7, 4];
        LOOP
            EXIT WHEN count = 0;
            count = count - 1;
            IF count%11 = 3 THEN
                CONTINUE;
            END IF;
            day = (SELECT generate_series('2016-08-15'::date, '2022-08-15'::date, '1 day'::interval) LIMIT 1);
            rows = (SELECT COUNT(*) FROM game_round);
            bett = ((count%11)+1)*5;
            game = 1+(count%11);
            adminn = 10160+(10*(count%11));

            INSERT INTO game_round VALUES
            (DEFAULT, null, game, gameplayers[game], bett,
            bett*gameplayers[game], day,
            null, adminn);
        END LOOP;
    END
$$;

CALL generate_gameround(50);

CREATE FUNCTION generate_series( t1 date, t2 date, i interval )
RETURNS setof date
AS $$
  SELECT d::date
  FROM generate_series(
    t1::timestamp without time zone,
    t2::timestamp without time zone,
    i
  )
    AS gs(d)
$$
LANGUAGE sql
IMMUTABLE;

CREATE OR REPLACE PROCEDURE generate_user_hist(count INT) LANGUAGE plpgsql AS $$
    DECLARE
        userr INT;
        roundd INT;
        all_users INT;
        c INT;
    BEGIN
        LOOP
            EXIT WHEN count = 0;
            count = count - 1;

            all_users = (SELECT COUNT(*) from "user");
            userr = (count%all_users)*10 + 100;
            c = (SELECT COUNT(*) FROM game_round);
            roundd = (count%c) +33;

            INSERT INTO player_in_roundgame VALUES
            (DEFAULT, roundd, userr, count%all_users + 4);
        END LOOP;
    END
$$;

CALL generate_user_hist(10000);

DELETE FROM player_in_roundgame WHERE attempt_id >=15;

CREATE OR REPLACE PROCEDURE generate_fin_operations(count INT) LANGUAGE plpgsql AS $$
    DECLARE
        userr INT;
        all_users INT;
        day DATE;
        services service ARRAY;
        exchanges operation ARRAY;
        transmissions operation ARRAY;
        withdrawals operation ARRAY;
        types motion ARRAY;
        rates INT;
        typee motion;
    BEGIN
        LOOP
            EXIT WHEN count = 0;
            count = count - 1;

            services = ARRAY ['QIWI', 'WebMoney', 'PayPal', 'Alipay'];
            exchanges = ARRAY['money on chips', 'chips on money'];
            transmissions = ARRAY['award'];
            withdrawals = ARRAY['penalty', 'bet'];
            types = ARRAY ['exchange', 'transmission', 'withdrawal'];
            typee = types[count%3 + 1];

            all_users = (SELECT COUNT(*) from "user");
            rates = (SELECT COUNT(*) from exchange_rate);
            userr = (count%all_users)*10 + 100;

            day = (SELECT generate_series('2016-08-15'::date, '2022-08-15'::date, '1 day'::interval) LIMIT 1);

            IF (typee = 'exchange') THEN
                INSERT INTO finance_operation VALUES
            (DEFAULT, (count%rates) + 14 ,userr, typee, services[count%4 + 1] , exchanges[count%2 + 1], (count%all_users+3)*5, day);
            end if;
            IF (typee = 'transmission') THEN
                INSERT INTO finance_operation VALUES
            (DEFAULT, (count%rates) + 14 ,userr, typee,services[count%4 + 1] , transmissions[1], (count%all_users+3)*5, day);
            end if;
            IF (typee = 'withdrawal') THEN
                INSERT INTO finance_operation VALUES
            (DEFAULT, (count%rates) + 14 ,userr, typee,services[count%4 + 1] , withdrawals[count%2 + 1], (count%all_users+3)*5, day);
            end if;

        END LOOP;
    END
$$;

CALL generate_fin_operations(1000);

CREATE OR REPLACE PROCEDURE generate_players(count INT) LANGUAGE plpgsql AS $$
    DECLARE
        userr INT;
        all_users INT;
        currencies currency_abr ARRAY;
    BEGIN
        currencies = ARRAY['CHIP', 'RUB', 'RUB', 'EUR', 'GBP', 'KRW', 'CNY', 'JPY'];
        LOOP
            EXIT WHEN count = 0;
            count = count - 1;

            all_users = (SELECT COUNT(*) from "user");
            userr = (count%all_users)*10 + 140;

            INSERT INTO player VALUES
            (userr, CONCAT('fic-player-', count, CAST(count as varchar(5))),
             (count%13) + (count%20)*100, currencies[(count%8) + 1], (count%13)*15, CONCAT('fictive-info-', count, CAST(count as varchar(5))));
        END LOOP;
    END
$$;

CALL generate_players(1000);
