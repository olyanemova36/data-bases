CREATE TRIGGER check_bet_on_chips
    BEFORE INSERT ON Finance_Operation
    FOR EACH ROW
    EXECUTE PROCEDURE check_account_update();

CREATE FUNCTION check_account_update() RETURNS TRIGGER AS $check_bet_on_chips$

    DECLARE
        needed_bet INT;
    BEGIN
       needed_bet = (   SELECT MIN(bet)
                        FROM Player_In_RoundGame INNER JOIN Game_Round ON round_id = Game_Round.id_game_round
                        INNER JOIN finance_operation ON finance_operation."user" = Player_In_RoundGame.user_id
                        WHERE finance_operation."user" = NEW."user"
                        GROUP BY "user");

        IF NEW.type != 'transmission' THEN
            RETURN NEW;
        END IF;

        IF needed_bet != NEW.sum THEN
            RAISE EXCEPTION '% INCORRECT VALUE FOR DEPOSITING', NEW.sum;
        END IF;

        IF NEW.sum IS NULL  THEN
            RAISE EXCEPTION '% CANNOT HAVE NULL SALARY', NEW.sum;
        END IF;

        IF NEW.sum < 0 THEN
            RAISE EXCEPTION '% CANNOT HAVE A NEGATIVE SALARY', NEW.sum;
        END IF;

        RETURN NEW;
    END
$check_bet_on_chips$ LANGUAGE plpgsql;

CREATE TRIGGER check_players
    BEFORE INSERT ON Player_In_RoundGame
    FOR EACH ROW
    EXECUTE PROCEDURE check_number_players();


CREATE FUNCTION check_number_players() RETURNS TRIGGER AS $Game_Round$

    DECLARE
        current_count  INT;
        required_count  INT;
    BEGIN
            current_count = (   SELECT COUNT(pir.user_id)
                                FROM Player_In_RoundGame pir JOIN Game_Round gr ON pir.round_id = gr.id_game_round
                                WHERE NEW.round_id = gr.id_game_round
                                GROUP BY round_id);

            required_count = (  SELECT MIN(member_count)
                                FROM Gametype JOIN game_round gr ON gametype.game_id = gr.played_game
                                WHERE gr.id_game_round = NEW.round_id);

        IF(current_count = required_count) THEN
            RAISE EXCEPTION 'YOU CANT INSERT MORE USERS ITS WILL BE OVERLOADED';
        END IF;

        IF(current_count > required_count) THEN
            RAISE EXCEPTION 'INCORRECT STATE YOU SHOULD CHECK IT';
        END IF;

        RETURN NEW;
    END
$Game_Round$ LANGUAGE plpgsql;

CREATE TRIGGER make_operation
    BEFORE INSERT ON Finance_Operation
    FOR EACH ROW
    EXECUTE PROCEDURE update_user_cash();

CREATE FUNCTION update_user_cash() RETURNS TRIGGER AS $make_operation$

    DECLARE
        in_currency  currency_abr;
        from_currency currency_abr;
    BEGIN

         in_currency = ( SELECT "in"
                         FROM exchange_rate INNER JOIN new on exchange_rate.id_change_rate = new.id_exchange_rate);

         from_currency = (  SELECT "from"
                            FROM exchange_rate INNER JOIN new on exchange_rate.id_change_rate = new.id_exchange_rate);

        if NEW.type == 'exchange' AND NEW.operation_type == THEN
            IF in_currency ==
        end if;

       needed_bet = (   SELECT MIN(bet)
                        FROM Player_In_RoundGame INNER JOIN Game_Round ON round_id = Game_Round.id_game_round
                        INNER JOIN NEW ON NEW."user" = Player_In_RoundGame.user_id
                        WHERE Player_In_RoundGame.user_id = NEW."user");

        fixed_bet_mean = 'depositing';
        IF NEW.operation_type != fixed_bet_mean THEN
           RETURN NEW;
        END IF;

        IF needed_bet != NEW.sum THEN
            RAISE EXCEPTION '% INCORRECT VALUE FOR DEPOSITING', NEW.sum;
        END IF;

        IF NEW.sum IS NULL  THEN
            RAISE EXCEPTION '% CANNOT HAVE NULL SALARY', NEW.sum;
        END IF;

        IF NEW.sum < 0 THEN
            RAISE EXCEPTION '% CANNOT HAVE A NEGATIVE SALARY', NEW.sum;
        END IF;

        RETURN NEW;
    END;
$make_operation$ LANGUAGE plpgsql;


SELECT user_id, begin
FROM Player_In_RoundGame pir JOIN Game_Round gr ON pir.round_id = gr.id_game_round
WHERE played_game = 9 AND begin > '2022-05-19 00:00:00.000000'::timestamp AND round_id = 84

SELECT player_id
                    FROM player p JOIN player_in_roundgame pir ON p.player_id = pir.user_id
                    JOIN game_round gr ON pir.round_id = gr.id_game_round
                    JOIN gametype g on gr.played_game = g.game_id
                    WHERE game_name = 'go' AND
                          NOT EXISTS(
                              SELECT '1'
                              FROM player JOIN player_in_roundgame pir ON player.player_id = pir.user_id
                              JOIN game_round gr ON pir.round_id = gr.id_game_round
                              JOIN gametype g on gr.played_game = g.game_id
                              WHERE game_name != 'go' AND player.player_id = p.player_id
                          )

SELECT player_id, "from", "in"
FROM player p JOIN finance_operation fo ON p.player_id = fo."user"
              JOIN exchange_rate er ON er.id_change_rate = fo.id_exchange_rate
WHERE player_id = 10180