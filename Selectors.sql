-- Выбрать всех игроков, которые играли в моджонг в 2016-08-15 со ставкой выше 35

explain analyze SELECT login
FROM "user" JOIN player_in_roundgame pir on "user".user_id = pir.user_id
            JOIN game_round  gr ON gr.id_game_round = pir.round_id
            JOIN gametype g on gr.played_game = g.game_id
WHERE game_name = 'majong' AND begin = '2016-08-15 00:00:00.000000'::timestamp AND bet >= 35;

-- Вывести игрока и количество его финансовых опеарций в фунтах стерлингах на азиатские валюты,
-- для игроков, кто играл только в го

CREATE INDEX moneyman
ON finance_operation("user");

CREATE INDEX playyer
ON player(player_id);

CREATE INDEX gamer
ON gametype(game_name);

DROP INDEX gamer;

EXPLAIN ANALYSE -- please explain me everything

WITH player_story (player_id, game_name)
AS
(
    SELECT player_id, game_name
    FROM player p JOIN player_in_roundgame pir ON p.player_id = pir.user_id
    JOIN game_round gr ON pir.round_id = gr.id_game_round
    JOIN gametype g on gr.played_game = g.game_id
)

SELECT nickname, COUNT(id_finance_op)
FROM player p JOIN finance_operation fo ON p.player_id = fo."user"
              JOIN exchange_rate er ON er.id_change_rate = fo.id_exchange_rate
WHERE er."in" = 'GBP' AND er."from" IN ('KRW', 'CNY', 'JPY') AND -- bye bye index scan
      player_id IN (SELECT player_id
                    FROM player_story ps
                    WHERE game_name = 'go' AND -- bye bye index scan
                          NOT EXISTS(
                              SELECT '1'
                              FROM player_story
                              WHERE game_name != 'go' AND ps.player_id = player_story.player_id -- bye bye index scan
                          )
      )
GROUP BY nickname;

SET enable_seqscan = OFF;
/**
  Asian money

    Planning Time: 1.890 ms
    Execution Time: 128.330 ms

    Added index for financial operation “user”

    Planning Time: 3.099 ms
    Execution Time: 126.961 ms

    Added index for player - player_id

    Planning Time: 7.477 ms
    Execution Time: 125.394 ms

    Deleted 1 returning value → *

    Planning Time: 4.849 ms
    Execution Time: 131.026 ms

    Added WITH statement (used a view)

    Planning Time: 1.177 ms
    Execution Time: 15.979 ms

    Added INDEX for gamename::varchar()

    Planning Time: 1.845 ms
    Execution Time: 24.668 ms
 */
-- Вывести всех игроков, c самым большим кошельком,
-- кто пользовался для финансовых операций любые сервисы, кроме сервисов PayPal

SELECT DISTINCT nickname, chips
FROM player p JOIN finance_operation fo ON p.player_id = fo."user"
WHERE chips = ( SELECT MAX(chips)
                FROM player) AND service != 'PayPal'
                AND NOT EXISTS (
                    SELECT '1'
                    FROM finance_operation fo
                    WHERE fo."user" = p.player_id AND fo.service = 'PayPal'
                );


-- Вывести рейтинг игр в онлайн-казино и процент в рейтинге, по популярности  в 2016 году
-- на основании количества проведенных турниров

SELECT game_name, CONCAT(ROUND((CAST(COUNT(id_game_round) AS DECIMAL(6,2))/(SELECT COUNT(round_id)
                    FROM gametype JOIN game_round gr on gametype.game_id = gr.played_game
                    JOIN player_in_roundgame pir ON pir.round_id = gr.id_game_round
                    WHERE begin = '2016-08-15 00:00:00.000000'::timestamp))*100, 1), ' %') as score
FROM gametype JOIN game_round gr on gametype.game_id = gr.played_game
JOIN player_in_roundgame pir ON pir.round_id = gr.id_game_round
WHERE begin = '2016-08-15 00:00:00.000000'::timestamp
GROUP BY game_name
ORDER BY COUNT(id_game_round) DESC;

-- Для каждого игрока вывести заработанную сумму на играх за все время пребывания в игре, имя игрока и
-- указать в процентах на сколько был увеличен за все время кошелек пользовтеля и средний процент зароботка в игре

CREATE INDEX gameplayer
ON player (player_id);


SELECT nickname, chips ,SUM(fo.sum) as revenue,
       CONCAT(ROUND(SUM(fo.sum)/CAST(chips AS DECIMAL(10,4))*100, 0), ' %') as increased,
       (SELECT AVG(fo.sum)
           FROM player p JOIN finance_operation fo ON p.player_id = fo."user") as avg_revenue,
       CONCAT(ROUND((SUM(fo.sum) / (SELECT AVG(fo.sum)
           FROM player p JOIN finance_operation fo ON p.player_id = fo."user"))*100, 0), ' %') as avg_increased
FROM player p JOIN finance_operation fo ON p.player_id = fo."user"
WHERE fo.operation_type = 'award' AND chips != 0
GROUP BY nickname, chips
ORDER BY revenue DESC