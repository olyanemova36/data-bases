CREATE TABLE S (  ---Seller
    Sid CHAR(4) NOT NULL UNIQUE PRIMARY KEY ,
    SName VARCHAR(20) NOT NULL UNIQUE,  --AK
    SCity VARCHAR(10) NOT NULL,
    Status INTEGER NOT NULL
);

CREATE TABLE B (  ---Bases
    Bid CHAR(4) UNIQUE NOT NULL PRIMARY KEY ,
    BName VARCHAR(20) NOT NULL UNIQUE,  --AK
    BCity VARCHAR(10) NOT NULL
);

CREATE TABLE G ( ---Goods
    Gid CHAR(4) UNIQUE NOT NULL PRIMARY KEY ,
    GName VARCHAR(20) NOT NULL UNIQUE,  --AK
    GCity VARCHAR(10) NOT NULL,
    Price DECIMAL(6, 2) NOT NULL,
    Weight INTEGER NOT NULL
);

CREATE TABLE SGB ( ---Goods
    Gid CHAR(4) NOT NULL REFERENCES G(Gid) ON UPDATE CASCADE ON DELETE RESTRICT ,
    Sid CHAR(4) NOT NULL REFERENCES S(Sid) ON UPDATE CASCADE ON DELETE RESTRICT ,
    Bid CHAR(4) NOT NULL REFERENCES B(Bid) ON UPDATE CASCADE ON DELETE RESTRICT ,

    PRIMARY KEY (Gid, Sid, Bid),
    Gty INTEGER NOT NULL
);

--- inserted data
INSERT INTO S VALUES
    ('aaa1', 'Dobrostroy', 'Novgorod', 3),
    ('aaa2', 'Selectel', 'Moscow', 1),
    ('aaa3', 'X5Retail', 'Voronezh', 1),
    ('aaa4', 'Ozon', 'Samara', 2),
    ('aaa5', 'Franchesko', 'Tanmbov', 2),
    ('aaa1', 'Prostokvashino', 'Yakutsk', 3),
    ('aaa7', 'Lubyatovo', 'Vologda', 3);

INSERT INTO S VALUES
    ('aaa8', 'Newcomer', 'Vologda', 1);

INSERT INTO B VALUES
    ('bbb1', 'Kukushka', 'Novgorod'),
    ('bbb2', 'Solomon', 'Moscow'),
    ('bbb3', 'MountK', 'Voronezh'),
    ('bbb4', 'Lieart', 'Lipetsk'),
    ('bbb5', 'Kamash', 'Kirovsk'),
    ('bbb6', 'Kuznets', 'Sochi');

INSERT INTO G VALUES
    ('ggg1', 'Milk', 'Novgorod', 100, 1),
    ('ggg2', 'Salt', 'Moscow', 30, 5),
    ('ggg3', 'Meat', 'Voronezh', 1800, 8),
    ('ggg4', 'Fish', 'Lipetsk', 1300, 7),
    ('ggg5', 'Weet', 'Kirovsk', 500, 30),
    ('ggg6', 'Juice', 'Lipetsk', 90, 2),
    ('ggg7', 'Grape', 'Sochi', 200, 3),
    ('ggg8', 'Apple', 'Sochi', 120, 12),
    ('ggg9', 'Orange', 'Moscow', 80, 15),
    ('gg10', 'Peach', 'Sochi', 140, 20);

DELETE FROM G;

INSERT INTO SGB VALUES
    ('ggg1', 'aaa4', 'bbb1', 1),
    ('ggg8', 'aaa3', 'bbb1', 5),
    ('ggg8', 'aaa4', 'bbb2', 3),
    ('ggg8', 'aaa6', 'bbb2', 12),
    ('ggg2', 'aaa4', 'bbb5', 4),
    ('ggg2', 'aaa7', 'bbb5', 3),
    ('ggg1', 'aaa7', 'bbb5', 6),
    ('ggg1', 'aaa5', 'bbb2', 77),
    ('ggg9', 'aaa1', 'bbb6', 122),
    ('ggg9', 'aaa1', 'bbb4', 6),
    ('ggg9', 'aaa3', 'bbb3', 3);

--- selected data
--- 1.Получить номера и имена поставщиков, не поставляющих никакие товары, и названия городов, в которых они дислоцированы.
--- checked [OK]
SELECT DISTINCT Sid, SName
FROM S s1
    WHERE NOT EXISTS (
        SELECT *
        FROM S s2 JOIN SGB on s2.Sid = SGB.Sid
        WHERE s1.Sid = s2.Sid);

--- 1. Получить название и цену тех товаров, цена которых превышает среднюю цену всех товаров.
-- [OK]

SELECT DISTINCT Gid, GName
FROM G g1
    WHERE Price >  (
        SELECT AVG(Price)
        FROM G
        );


--- 2. Получить номера и названия баз и количество разных видов товаров,
-- поставляемых на эти базы (учесть и базы, на которые
-- товары не поставляются).
--- [OK]

SELECT DISTINCT B.Bid, BName, COUNT(DISTINCT Gid)
FROM B LEFT JOIN SGB on B.Bid = SGB.Bid
GROUP BY B.Bid, BName;

--- 3. Получить названия торговых баз, поставки на которые выполняются более чем одним поставщиком.
--- [OK]

SELECT DISTINCT BName
FROM B LEFT JOIN SGB on B.Bid = SGB.Bid
GROUP BY BName
HAVING COUNT( DISTINCT Sid) > 1;

--- 4. Получить номера и названия баз, для которых среднее
-- количество поставляемых в одной поставке товаров с кодом G1 больше,
-- чем максимальное количество любых товаров, поставляемых в одной
-- поставке на торговую базу с номером B1.
--- [OK]

WITH MADD(Bid, count)
    AS (
        SELECT Bid, COUNT(DISTINCT Gid)
        FROM SGB
        WHERE Bid = 'bbb1'
        GROUP BY Bid
    )

SELECT DISTINCT SGB.Bid, BName
FROM B B1 LEFT JOIN SGB on B1.Bid = SGB.Bid
WHERE (SELECT AVG(Gty)
        FROM B B2 LEFT JOIN SGB on B2.Bid = SGB.Bid
        WHERE Gid = 'ggg1' AND B2.Bid = B1.Bid ) > (SELECT MAX(MADD.count )
                                                    FROM MADD)
GROUP BY SGB.Bid, BName;


--- 5. Получить имена поставщиков, получивших минимальный
-- доход от поставок товаров (доход от поставки товара определяется как
-- произведение суммарного количества поставленного товара на его
-- цену).
--- [OK]

WITH MSUM (SName, SSum)
AS
         (
            SELECT SName, SUM(Gty * Price)
            FROM S JOIN SGB on S.Sid = SGB.Sid
            JOIN G on SGB.Gid = G.Gid
            GROUP BY SName
         )

SELECT SName
FROM S JOIN SGB on S.Sid = SGB.Sid
    JOIN G on SGB.Gid = G.Gid
GROUP BY SName
HAVING SUM(Gty * Price) = (SELECT MIN(MSUM.SSum)
                            FROM MSUM);



