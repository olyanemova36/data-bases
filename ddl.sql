CREATE TABLE Gametype (
    game_id INT GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1) NOT NULL PRIMARY KEY,
    game_name CHAR(30) NOT NULL UNIQUE,
    description VARCHAR(100) NULL,
    member_count INT NOT NULL CHECK (member_count > 0) DEFAULT(3)
);

CREATE TABLE Exchange_rate (
    id_change_rate INT GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1) NOT NULL PRIMARY KEY,

    "in" CURRENCY_ABR NOT NULL,
    "from" CURRENCY_ABR NOT NULL,
    date DATE NOT NULL,

    CHECK ("in" <> "from"),
    UNIQUE ("in", "from", date),

    value DECIMAL(12,4) NOT NULL CHECK (value >= 0.0)
);

CREATE TYPE USERTYPE AS ENUM ('player', 'admin', 'violator');
CREATE TYPE OPERATION AS ENUM ('money on chips', 'chips on money', 'award', 'penalty', 'bet');
CREATE TYPE MOTION AS ENUM ('exchange', 'transmission', 'withdrawal');
CREATE TYPE SERVICE AS ENUM ('QIWI', 'WebMoney', 'PayPal', 'Alipay');
CREATE TYPE CURRENCY_ABR AS ENUM ('CHIP', 'RUB', 'USD', 'EUR', 'GBP', 'KRW', 'CNY', 'JPY');

CREATE TABLE "user" (
    user_id INT GENERATED ALWAYS AS IDENTITY (START WITH 100 INCREMENT BY 10) NOT NULL PRIMARY KEY,
    login CHAR(15) NOT NULL UNIQUE,
    user_type USERTYPE NOT NULL,
    avatar VARCHAR(227) NULL,

    hashed_password VARCHAR(20) NOT NULL,
    salt VARCHAR(24) NOT NULL
);

CREATE INDEX user_index ON "user"(login);

CREATE TABLE Player (
    player_id INT NOT NULL REFERENCES "user"(user_id) ON DELETE CASCADE ON UPDATE CASCADE PRIMARY KEY,
    nickname CHAR(20) NOT NULL UNIQUE,
    money DECIMAL NOT NULL CHECK (money >= 0) DEFAULT(0.0),
    currency CURRENCY_ABR NOT NULL DEFAULT ('USD'),
    chips NUMERIC NOT NULL CHECK (chips % 5 = 0 AND chips >= 0) DEFAULT(0),
    additional_info VARCHAR(100) NULL
);

CREATE INDEX player_index ON Player(nickname);

CREATE TABLE Administrator (
    admin_id INT NOT NULL REFERENCES "user"(user_id) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY(admin_id),
    count_blocked_players INT NOT NULL DEFAULT(0),
    adm_number INT NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1000 INCREMENT BY 1)
);

CREATE TABLE Finance_Operation (
    id_finance_op INT GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1) NOT NULL UNIQUE PRIMARY KEY,

    id_exchange_rate INT NULL REFERENCES Exchange_rate(id_change_rate) ON DELETE RESTRICT ON UPDATE CASCADE,
    "user" INT NOT NULL REFERENCES Player(player_id) ON DELETE CASCADE ON UPDATE CASCADE,

    type MOTION NOT NULL,
    service SERVICE NOT NULL,
    operation_type OPERATION NOT NULL,
    value DECIMAL NOT NULL CHECK (value > 0),
    date DATE NOT NULL
);

CREATE TABLE Game_Round (
    id_game_round INT GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1) NOT NULL PRIMARY KEY,
    include INT NULL REFERENCES Game_Round(id_game_round) ON DELETE CASCADE ON UPDATE CASCADE,
    played_game INT NOT NULL REFERENCES Gametype(game_id) ON DELETE CASCADE ON UPDATE CASCADE,
    current_players NUMERIC NOT NULL CHECK (current_players > 0),
    bet DECIMAL NOT NULL CHECK (bet % 5 = 0 AND bet > 0) DEFAULT(5),
    prize NUMERIC NOT NULL CHECK (prize = bet * Game_Round.current_players AND prize > 0) DEFAULT(5),
    begin TIMESTAMP NOT NULL,
    duration INTERVAL HOUR TO MINUTE NULL,
    admin INT NOT NULL REFERENCES Administrator(admin_id) ON DELETE SET DEFAULT ON UPDATE CASCADE
);

CREATE TABLE Player_In_RoundGame (
    attempt_id INT GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1) NOT NULL PRIMARY KEY,
    round_id INT NULL REFERENCES Game_Round(id_game_round) ON DELETE CASCADE ON UPDATE CASCADE,
    user_id INT NOT NULL REFERENCES Player(player_id) ON DELETE CASCADE ON UPDATE CASCADE,
    attempt_num INT NOT NULL
);

CREATE TABLE Blocker_Player (
    blocked_player_id INT NOT NULL REFERENCES Player(player_id) ON DELETE CASCADE ON UPDATE RESTRICT,
    PRIMARY KEY (blocked_player_id),
    admin INT NOT NULL REFERENCES Administrator(admin_id) ON DELETE RESTRICT ON UPDATE RESTRICT,
    violation VARCHAR(100) NOT NULL
);

CREATE INDEX violator_index ON Blocker_Player(blocked_player_id);
